//'use strict';
var http = require('http');

var totalBytes = 0;

console.log('getting main page');
//get the root
var req = http.request(
{
    hostname: '127.0.0.1',
    port: 80,
    path: '/',
    method: 'GET'
}, function(res)
{
    var data = '';
    //now we have the response from the page
    res.on('data', function(chunk)
    {
        data += chunk;
    });
    res.on('end', function()
    {
        console.log('main page gotten');
        processHomePage(data);
    });
});
req.end();


function processHomePage(text)
{
    var rows = text.split('\n');
    for(var i = 0; i < rows.length; i++)
    {
        var find = /<a class='chapter media thumbnail' href='(.*?)' id='.*?'>/.exec(rows[i]);
        if(find)
        {
           console.log('found chapter: ' + find[1]);
           processPage(find[1]);
        }
    }
}

function processPage(pagePath)
{
    var req = http.request(
    {
        hostname: '127.0.0.1',
        port: 80,
        path: pagePath,
        method: 'GET'
    }, function(res)
    {
        console.log('ressin: ' + pagePath);
        var data = '';
        //now we have the response from the page
        res.on('data', function(chunk)
        {
            data += chunk;
        });
        res.on('end', function()
        {
            console.log('got chapter data');
            var rows = data.split('\n');
            for(var i = 0; i < rows.length; i++)
            {
                if(rows[i].indexOf('var pages') !== -1)
                {
                    console.log('gettn pages');
                    //var pages = eval(/[.(]/.exec(rows[i])[1]);
                    eval(rows[i]);
                    //console.log(pages);
                    pages.forEach(function(page)
                    {
                        setTimeout(function()
			{
			    slurp(page.image);
			}, Math.random() * 100000);
                    });
                }
            }
        });
    });
    req.end();
}

function slurp(path)
{
    path = path.replace(/[' ]/g, escape)
    console.log('slurpin: ' + path);
    var req = http.request(
    {
        hostname: '127.0.0.1',
        port: 80,
        path: path,
        method: 'GET'
    }, function(res)
    {   
        if(res.statusCode == 404) {console.log('Error: 404 on ' + path);}
        res.on('data', function(chunk){totalBytes += chunk.length;});
        res.on('end', function()
        {
            console.log('finished slurpin: ' + path);
            console.log('current slurped: ' + totalBytes + ' bytes');
        });
    }
    );
    req.end();
}
